export interface Telephone {
    number: number;
    area_code: number;
    country_code: number;
    telephone_type: string;
    client_id: number;
}

