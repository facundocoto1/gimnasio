export interface Category {
    name: string;
    description:	string;
    picture?: string;
    key$?: string;
}
