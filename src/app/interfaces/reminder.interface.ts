export interface Reminder {
    id: number;
    start_date: string;
    end_date: string;
    description: string;
    state: string;
    reminder_type: string;
    last_modifier: number;
    allowed_users: number [];
}
