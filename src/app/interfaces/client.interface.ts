export interface Client {
    id: number;
    name: string;
    lastname: string;
    birthdate: string;
    number_identity_card: string;
    picture: string;
    observaciones: string;
    email: string;
    sex: string;
    unsubscription_date?: string;
    subscription_date: string;
    address?: number;
    expiration_date: number;
    categories: number[];
    phones?: number[];
    key$?: string;
}
