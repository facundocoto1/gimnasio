import { AuthService } from './auth.service';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';

@Injectable()

export class BillsService {
  token= '';

// Server URL
  billsURL = 'http://localhost:8000/api/v1/cuotas';

  constructor( private _http: Http,
               private _auth: AuthService ) {
                 this.token = this._auth.returnToken();
  }

// Method to get alls bills whitout payment date
  getBillsUnpaid() {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?pagada=false`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Method to get all bills whit payment date
  getBillsPaid() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this._http.get(this.billsURL, {headers})
    .map(res => res.json()
    );
  }

// Method to get all expired bills
  getBillsExpired() {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?vencida=true`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Metod to get all expired bills for one client
  getBillsExpiredClient(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?cliente=${key$}&vencida=true`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Method to gett all paid bills for one client
  getBillsPaidClient(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?cliente=${key$}&pagada=true`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Method to get all unpaid bills for one client
  getBillsUnpiadClient(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?cliente=${key$}&pagada=false`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Method to get one bill by id
  getBill(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/?id=${key$}`;
    return this._http.get(url, {headers})
    .map(res => res.json()
    );
  }

// Method to update one bill
  actualizarCuota(cuota: any) {
    const body = cuota;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });
    const url = `${ this.billsURL }/`;
    const alpha: any = [];
    alpha.push(body);
    return this._http.put( url, alpha, {headers} );
  }

}
