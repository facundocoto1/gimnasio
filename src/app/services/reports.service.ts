import { AuthService } from './auth.service';
import { Http, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ReportsService {

// Server url's
  reportClientURL = 'http://localhost:8000/api/v1/reportes?fecha_inicio=2000-06-01&fecha_fin=2017-12-01';
  reportBillURL = 'http://localhost:8000/api/v1/reportes/cuotas?fecha_inicio=2017-01-01&fecha_fin=2017-12-31';
  token = '';

  constructor( private _http: Http,
               private _auth: AuthService ) {
                   this.token = this._auth.returnToken();
  }

// Method to get clients reports
  getReportsClients() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this._http.get(this.reportClientURL, {headers})
    .map(res => res.json()
    );
  }

// Method to get bills reports
  getReportsBills() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this._http.get(this.reportBillURL, {headers})
    .map(res => res.json()
    );
  }
}
