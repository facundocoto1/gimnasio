import { AuthService } from './auth.service';
import { User } from '../interfaces/user.interface';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';


@Injectable()
export class UsersService {
  // Variable that contains token of session
  token= '';
   // Server URL
   userURL = 'http://localhost:8000/api/v1/usuarios';

  constructor(private _http: Http,
              private _auth: AuthService ) {
                this.token = this._auth.returnToken();
   }

// Method to create new User
    newUser(user: User) {
      const body = JSON.stringify(user);
      const headers = new Headers({
        'Content-type': 'application/json'
      });
      const url = `${ this.userURL }/`;
      return this._http.post( url, body, {headers} );
    }

// Method to update a  user
    updateUser(user: any) {
      const body = JSON.stringify(user);
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token
      });
       const url = `${ this.userURL }/`;
      return this._http.put( url, body, {headers} );
    }

// Method to get a user by id
    getUser(key$: string) {
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token
      });
       const url = `${ this.userURL }?username=${key$}`;

      return this._http.get(url, {headers})
      .map(res => res.json()
      );
    }

// Method to delete a user by id
    deleteUser(key$: string) {
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token
      });
      const url = `${ this.userURL }/${key$}.json`;
      return this._http.delete( url )
       .map( res => res.json() );
    }

// Method to login user
    loginUser(user: any) {
       const body = JSON.stringify(user);
       const headers = new Headers({
         'Content-type': 'application/json'
       });
       const url = `${ this.userURL }/login/`;
       return this._http.post(url, body, {headers} )
         .map(res => {
           return res.json();
         });
    }

// Method to logout a user
    logoutUser(user: any) {
      const body = JSON.stringify(user);
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token

      });
      const url = `${ this.userURL }/logout/`;
      return this._http.post(url, body, {headers} )
        .map(res => {
          return res.json();
        });
   }

}
