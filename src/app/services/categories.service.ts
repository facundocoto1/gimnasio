import { AuthService } from './auth.service';
import { Http, Headers } from '@angular/http';
import { Category } from '../interfaces/category.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class CategoriesService {
  token= '';
  // Server URL:
  categoriesURL = 'http://localhost:8000/api/v1/categorias/';

  constructor( private _http: Http,
               private _auth: AuthService ) {
                 this.token = this._auth.returnToken();
  }

// Method to add a new category
  newCategory(category: Category) {
    const body = JSON.stringify(category);
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.token
    });
    return this._http.post( this.categoriesURL, body, {headers} )
      .map(res => {
        return res.json();
      });
  }

// Method to update category
  updateCategory(category: Category, key$: string) {
    const body = category;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });

     const url = `${ this.categoriesURL }`;
     const alpha = new Array();
     alpha.push(body);

    return this._http.put( url, alpha, {headers} );
  }

// Method to get one category
  getCategory(key$: string) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.categoriesURL }?id=${key$}`;
    return this._http.get(url, {headers})
    .map(res => {
      console.log(res.json());
      return res.json();
    });
  }

// Method to get all categories
  getCategories() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this._http.get(this.categoriesURL, {headers})
    .map(res => res.json()
    );
  }

}
