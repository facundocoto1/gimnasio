import { AuthService } from './auth.service';
import { Client } from '../interfaces/client.interface';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// para la función map
import 'rxjs/Rx';

@Injectable()
export class ClientsService {

 token= '';
// Server URL:
 clientsURL = 'http://localhost:8000/api/v1/clientes/';

 constructor( private _http: Http,
              private _auth: AuthService ) {
                    this.token = this._auth.returnToken();
            }
// Method to create a new client
    newClient(client: Client) {
      new Date(client.birthdate).toISOString().slice(0, 10);
      const body = JSON.stringify(client);
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token
      });

      return this._http.post( this.clientsURL, body, {headers} )
        .map(res => {
          console.log(res.json());
          return res.json();
        });
    }

// Method to update client
    updateClient(client: Client, key$: string) {
      new Date(client.birthdate).toISOString().slice(0, 10);
      const body = JSON.stringify(client);
      const headers = new Headers({
        'Content-type': 'application/json',
        'Authorization': this.token
      });
      const url = `${ this.clientsURL }`;
      return this._http.put( url, body, {headers} );
    }

// Method to get one client
    getCliente(key$: string) {
      const headers = new Headers({
        'Authorization': this.token
      });
       const url = `${ this.clientsURL }?id=${key$}`;
       return this._http.get(url, {headers})
       .map(res => {
         return res.json();
       });
    }

// Method to get all clients
    getClients() {
      const headers = new Headers({
        'Authorization': this.token
      });
      return this._http.get(this.clientsURL, {headers})
      .map(res => res.json()
      );
    }

// Method to delete one client
    deleteClient(key$: string) {
      const url = `${ this.clientsURL }?id=${key$}`;
      return this._http.delete( url )
       .map( res => res.json() );
    }

// Method to search clients by any fields
    searchClient(key$: string) {
      const headers = new Headers({
        'Authorization': this.token
      });
       const url = `${ this.clientsURL }?any=${key$}`;
       return this._http.get(url, {headers})
       .map(res => {
         console.log(res.json());
         return res.json();
       });
    }

}
