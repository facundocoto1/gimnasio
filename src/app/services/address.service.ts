import { AuthService } from './auth.service';
import { Address } from '../interfaces/address.interface';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AddressService {
  token= '';

// Server URL:
  addressURL = 'http://localhost:8000/api/v1/clientes/domicilios/';

  constructor( private _http: Http,
               private _auth: AuthService ) {
                 this.token = this._auth.returnToken();
  }

// Method to create new address
  newAddress(domicilio: Address, key$: number) {
   const body = JSON.stringify(domicilio);
   const headers = new Headers({
    'Content-type': 'application/json',
    'Authorization': this.token
  });

    const url = `${ this.addressURL }${key$}/`;
   return this._http.post( url, body, {headers} )
     .map(res => {
       console.log(res.json());
       return res.json();
     });
 }

// Method to update address by id
  updateAddress(domicilio: Address, key$: string) {
    const body = JSON.stringify(domicilio);
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });
    const url = `${ this.addressURL }${key$}/`;

    return this._http.put( url, body, {headers} );
  }

// Method to get address by id
  getAddress(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.addressURL }${key$}`;
    return this._http.get(url, {headers})
    .map(res => {
      return res.json();
    });
  }

}
