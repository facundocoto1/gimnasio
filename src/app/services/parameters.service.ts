import { AuthService } from './auth.service';
import { Settings } from '../interfaces/settings.interface';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ParametersService {
  token = '';
// SERVER URL
parametersURL = 'http://localhost:8000/api/v1/parametros';



  constructor( private _http: Http,
               private _auth: AuthService ) {
                 this.token = this._auth.returnToken();
  }


// Method to update parameters
  updateParameters(parametros: any) {
    const body = parametros;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });
    console.log(body);
    const url = `${ this.parametersURL }/`;

    return this._http.put( url, body, {headers} );
  }

// Method to get all parameters
  getParametros() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this._http.get(this.parametersURL, {headers})
    .map(res => res.json()
    );
  }

}
