import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {
  // Variable that contains if an user is logged
  isUserLoggedIn = false;


  constructor(private _router: Router) {
    this.isUserLoggedIn = false;
  }

  // Set user logged
  public setUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  // Get user logged
  public getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  // Method to renew time of session
  public refreshSession() {
    const expiresAt = JSON.stringify((5000 * 1000) + new Date().getTime());
    localStorage.setItem('expires_at', expiresAt);
  }

  // User login. Save token and start session
  public login(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expireIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.token);
    localStorage.setItem('expires_at', expiresAt);
  }

  // Close sesion, remove tokens and time of session
  public logout(): void {
    localStorage.clear();
    window.location.reload();
    this._router.navigate(['/']);
  }

  // Ask if session expired
  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  // Method that return token saved
  public returnToken(): string {
    let token = '';
    return  token = localStorage.getItem('access_token');
  }


}
