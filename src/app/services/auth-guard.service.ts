import { UsersService } from './users.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private _auth: AuthService,
              private _route: Router) { }

  // Methot to comprobe if can navigate an specific route
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {
    if (this._auth.isAuthenticated() === false) {
      this._route.navigate(['./login']);
    } else {
           return this._auth.isAuthenticated();
    }
  }

}
