import { AuthService } from './auth.service';
import { Telephone } from '../interfaces/telephone.interface';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TelephonesService {
token = '';

// Server URL:
telephonesURL = 'http://localhost:8000/api/v1/clientes/telefonos';

  constructor( private _http: Http,
               private _auth: AuthService ) {
                 this.token = this._auth.returnToken();
  }


// Method to create new telephone for a client
  newTelephone(telefono: Telephone, key$: number) {
    const body = telefono;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });
    const url = `${ this.telephonesURL }/${key$}/`;
    const alpha: any [] = [];
    alpha.push(body);

    return this._http.post( url, alpha, {headers} )
      .map(res => {
        return res.json();
      });
 }

// Method to get update telephone by id
  actualizarTelefono(telefono: Telephone, key$: string) {
    // Extract client of telephone
    const tel = {
      id: 0,
      numero: 0,
      codigo_pais: 0,
      codigo_area: 0,
      tipo: ''
    };

    tel.id = Number(key$);
    tel.numero = telefono.number;
    tel.codigo_area = telefono.area_code;
    tel.codigo_pais = telefono.country_code;
    tel.tipo = telefono.telephone_type;

    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });

    const url = `${ this.telephonesURL }/${key$}/`;
    const alpha: any = [];
    alpha.push(tel);
    return this._http.put( url, alpha, {headers} );
  }

// Method to get telephone by id
  getTelefono(key$: number) {
    const headers = new Headers({
      'Authorization': this.token
    });
     const url = `${ this.telephonesURL }/${key$}/`;
    return this._http.get(url, {headers})
    .map(res => {
      console.log(res.json());
      return res.json();
    });
  }


}
