import { AuthService } from './auth.service';
import { Reminder } from '../interfaces/reminder.interface';
import { Http, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';




@Injectable()
export class RemindersService {
// Server URL
 remindersURL = 'http://localhost:8000/api/v1/recordatorios/';
 token = '';

  constructor( private http: Http,
               private auth: AuthService) {
              this.token = this.auth.returnToken();
  }

// Method to create new reminder
  newReminder(recordatorio: Reminder) {
    new Date(recordatorio.start_date).toISOString().slice(0, 10);
    new Date(recordatorio.end_date).toISOString().slice(0, 10);
    const body = JSON.stringify(recordatorio);
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });

    return this.http.post( this.remindersURL, body, {headers} )
      .map(res => {
        console.log(res.json());
        return res.json();
      });
  }

// Method to update reminder
  updateReminder(recordatorio: any) {
    new Date(recordatorio.fecha_inicio).toISOString().slice(0, 10);
    new Date(recordatorio.fecha_fin).toISOString().slice(0, 10);
    const body = recordatorio;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });

    const url = `${ this.remindersURL }`;
    const alpha: any = [];
    alpha.push(body);
    return this.http.put( url, alpha, {headers} );
  }

// Method to cancel a reminder
  cancelReminder(recordatorio: Reminder) {
    new Date(recordatorio.start_date).toISOString().slice(0, 10);
    new Date(recordatorio.end_date).toISOString().slice(0, 10);
    recordatorio.state = 'resuelto';
    const body = recordatorio;
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': this.token
    });

    const url = `${ this.remindersURL }`;

    const alpha: any = [];
    alpha.push(body);
    return this.http.put( url, alpha, {headers} );
  }

// Method to get a reminder by id
  getReminder(key$: number): Observable<Reminder> {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.remindersURL }?id=${key$}`;
    return this.http.get(url, {headers})
    .map(res => {
      return res.json();
    });
  }

// Method to get all reminders
  getRecordatorios() {
    const headers = new Headers({
      'Authorization': this.token
    });
    return this.http.get(this.remindersURL, {headers})
    .map(res => res.json()
    );
  }

// Method to get all reminders by search
  getRemindersSearch(consulta: string) {
    const headers = new Headers({
      'Authorization': this.token
    });
    const url = `${ this.remindersURL }?estado=${consulta}`;
    return this.http.get(url, {headers})
    .map(res => res.json()
    );
  }

}
