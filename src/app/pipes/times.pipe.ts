import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'times',
  pure: false,
})
export class TimesPipe implements PipeTransform {

  transform(value: any): any {
    let parts = value.split(':');
    let hora  = parts[0];
    let minuto = parts[1];
    return hora + ':' + minuto;
  }

}
