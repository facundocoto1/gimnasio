import { RemindersService } from '../../services/reminders.service';
import { Component, OnInit } from '@angular/core';
import {Reminder} from '../../interfaces/reminder.interface';


@Component({
  selector: 'app-recordatorios',
  templateUrl: './recordatorios.component.html',
})
export class RecordatoriosComponent implements OnInit {
  recordatorio: any;
  recordatorioCancelado: Reminder;
  recordatorios: any [];
  loading = true;
  archiva = true;

  constructor( private _recordatoriosServices: RemindersService) {  }
  ngOnInit() {
    this._recordatoriosServices.getRecordatorios().subscribe(data => {
      this.recordatorios = data;
      this.loading = false;
      console.log(localStorage.getItem('access_token'));
    });

  }

  cancelarRecordatorio (key$: number) {
    console.log(key$);
    this._recordatoriosServices.getReminder(key$).subscribe(
      data => {this.recordatorioCancelado = data[0];
                console.log(this.recordatorioCancelado);
                this._recordatoriosServices.cancelReminder(this.recordatorioCancelado)
                .subscribe( res => console.log(res.json()));
               }
      );
}

consultar (key$: string) {
  this.archiva = false;
  this._recordatoriosServices.getRemindersSearch(key$).subscribe(data => {this.recordatorios = data;
    console.log(this.recordatorios); });
}

}
