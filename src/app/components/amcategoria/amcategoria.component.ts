import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from '../../interfaces/category.interface';
import { CategoriesService } from '../../services/categories.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-amcategoria',
  templateUrl: './amcategoria.component.html'
})
export class AmcategoriaComponent implements OnInit {

categoria: Category = {
    name: '',
    description:	'',
};
// as
nuevo = false;
id: string;

  constructor( private _categoriaService: CategoriesService,
               private router: Router,
               private route: ActivatedRoute,
               private auth: AuthService ) {

                this.auth.refreshSession();
                this.route.params.subscribe(
                   parametros => {
                     if (parametros.id == null) {
                      this.id = 'nuevo';
                     } else {
                      this.id = parametros['id'];
                      this._categoriaService.getCategory(this.id).subscribe( data => this.categoria = data[0] );
                     }
                   });
               }

  ngOnInit() {}

  // Method to save or update category
  guardar() {
     if (this.id === 'nuevo' ) {
      // insertando
      this._categoriaService.newCategory( this.categoria)
      .subscribe(data => { console.log(data);
                if (data['status'] === 201) {
                  alert('Categoria creada con exito');
                }
                },
                 error => alert('Hubo un error')); }
     // tslint:disable-next-line:one-line
     else {
      // actualizando
      this._categoriaService.updateCategory( this.categoria, this.id )
      .subscribe(
        data => {
          if (data['status'] === 200) {
            alert('Categoria actualizada con exito');
          }
    },
        error => alert('Hubo un error'));
    }
     this.router.navigate(['/settings']);
  }
}
