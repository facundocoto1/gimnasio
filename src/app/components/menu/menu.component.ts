import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
})
export class MenuComponent implements OnInit {

  constructor(private auth: AuthService) {

  }

  ngOnInit() {
  }


  salir() {
    this.auth.logout();
    console.log(this.auth.isAuthenticated());
  }

}
