import { TelephonesService } from '../../services/telephones.service';
import { AddressService } from '../../services/address.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientsService } from './../../services/clients.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
})
export class ClienteComponent {

  dadoDeBaja = false;

  cliente: any = {
  };
  domicilio: any = {};
  telefonos: any = {};
  id: string;

  constructor(  private activatedRoute: ActivatedRoute,
                private router: Router,
                private _clienteService: ClientsService,
                private _domicilioService: AddressService,
                private _telefonoService: TelephonesService,
                private auth: AuthService ) {
                  // Refresco la sesion
                this.auth.refreshSession();
                this.activatedRoute.params.subscribe(
                  parametros => {
                    this.id = parametros['id'];
                    this._clienteService.getCliente(this.id).subscribe( client => {
                      this.cliente = client[0];
                      if (this.cliente.fecha_baja != null) {
                        this.dadoDeBaja = true;
                      }
                      // ID cliente para buscar telefono y domicilio
                      console.log(this.cliente.id);
                      this._domicilioService.getAddress(this.cliente.id).subscribe (dom => {
                                                                                  this.domicilio = dom;
                                                                                  console.log(this.domicilio); });
                      this._telefonoService.getTelefono(this.cliente.id).subscribe (tel => {
                                                                                    this.telefonos = tel;
                                                                                    console.log(this.telefonos); });

                    });
                  }
                );

   }
}
