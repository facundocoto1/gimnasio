import { ReportsService } from '../../services/reports.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit {
  // OBJETOS REPORTES
  reportesPersonas: any [];
  reportesCuotas: any[];

  // Variables graficos
  cantHombres = 0;
  cantMujeres = 0;
  cantIndet = 0;

  edades = [];

  altas = 0;
  bajas = 0;

  categorias= [];
  categoriasValores = [];
  categoriaLabels: string[];
  valores: Array<number> = [];

  imapagas = [];
  facturadas = [];
  vencidas = [];

  valoresImpagas: Array<number> = [];
  valoresFacturadas = [];
  valoresVencidas: Array<number> = [];

   // SEXO
   public doughnutChartLabels: string[] = ['Mujeres', 'Hombres', 'Indeterminado'];
   public doughnutChartData: number[] = [this.cantMujeres, this.cantHombres, this.cantIndet];
   public doughnutChartType = 'doughnut';

  // CATEGORIAS
  // GRAFICO PIE
  public pieChartLabels: string[] = this.categoriaLabels;
  public pieChartData: number[] = this.valores;
  public pieChartType = 'pie';

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };


  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  // EDADES
  // tslint:disable-next-line:member-ordering
  public barChartOptions2: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  // setear con los datos que vengan
  // tslint:disable-next-line:member-ordering
  public barChartLabels2: string[] = ['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60+'];
  // tslint:disable-next-line:member-ordering
  public barChartType2 = 'bar';
  // tslint:disable-next-line:member-ordering
  public barChartLegend2 = false;

  // tslint:disable-next-line:member-ordering
  public barChartData2: any[] = [
    {data: [this.edades], label: ''}
  ];


  // CUOTAS
  // tslint:disable-next-line:member-ordering
  public barChartOptions3: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  // setear con los datos que vengan
  // tslint:disable-next-line:member-ordering
  public barChartLabels3: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
                                      'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  // tslint:disable-next-line:member-ordering
  public barChartType3 = 'bar';
  // tslint:disable-next-line:member-ordering
  public barChartLegend3 = true;

  // tslint:disable-next-line:member-ordering
  public barChartData3: any[] = [
    {data: this.valoresFacturadas, label: 'Ipmagas'},
    {data: this.valoresFacturadas, label: 'Vencidas'},
    {data: this.valoresFacturadas, label: 'Facturadas'}
  ];



  constructor(private _reportesService: ReportsService) {
      }
      ngOnInit() {
        this._reportesService.getReportsClients().subscribe(data => {this.reportesPersonas = data;
                                                              // Lleno valores reporte sexo
                                                              this.cantHombres = this.reportesPersonas['sexo']['Masculino'];
                                                              this.cantMujeres = this.reportesPersonas['sexo']['Femenino'];
                                                              this.cantIndet = this.reportesPersonas['sexo']['Indeterminado'];
                                                              this.doughnutChartData = [this.cantMujeres,
                                                                                        this.cantHombres,
                                                                                        this.cantMujeres];

                                                              // Lleno valores reporte edades
                                                              this.edades[0] = Number(this.reportesPersonas['edad']['0 - 10']);
                                                              this.edades[1] = Number(this.reportesPersonas['edad']['10 - 20']);
                                                              this.edades[2] = Number(this.reportesPersonas['edad']['20 - 30']);
                                                              this.edades[3] = Number(this.reportesPersonas['edad']['30 - 40']);
                                                              this.edades[4] = Number(this.reportesPersonas['edad']['40 - 50']);
                                                              this.edades[5] = Number(this.reportesPersonas['edad']['50 - 60']);
                                                              this.edades[6] = Number(this.reportesPersonas['edad']['Mayor a 60']);
                                                              this.barChartData2 = this.edades;

                                                              // LLeno valores altas bajas
                                                              this.altas = this.reportesPersonas['altas-bajas']['Altas'];
                                                              this.bajas = this.reportesPersonas['altas-bajas']['Bajas'];

                                                              // Leno valores categorias
                                                              this.categorias = this.reportesPersonas['categoria'];
                                                              // Separo key value
                                                              const data1 = [];
                                                              const data2 = [];
                                                              for (const property in this.categorias) {
                                                                if ( ! this.categorias.hasOwnProperty(property)) {
                                                                    continue;
                                                                }
                                                                data1.push(property);
                                                                data2.push(this.categorias[property]);
                                                              }
                                                              // Convierto a numero los valores
                                                              const data3 = data2.map(Number);
                                                              // Asigno valores a grafico
                                                              this.categoriaLabels = data1;
                                                              this.pieChartLabels = this.categoriaLabels;

                                                              this.categoriasValores = data3;

                                                              for (const valor in this.categoriasValores){
                                                                this.valores.push(this.categoriasValores[valor]);
                                                              }

                                                            });
        this._reportesService.getReportsBills().subscribe(data => {this.reportesCuotas = data;
                                                            this.imapagas = this.reportesCuotas['impagas'];
                                                            this.facturadas = this.reportesCuotas['facturadas'];
                                                            this.vencidas = this.reportesCuotas['vencidas'];

                                                            for (let valor in this.imapagas){
                                                              this.valoresImpagas.push(this.imapagas[valor]['valor_total']);
                                                            }

                                                            for (let valor in this.facturadas){
                                                              this.valoresFacturadas.push(this.facturadas[valor]['valor_total']);
                                                            }

                                                            for (let valor in this.vencidas){
                                                              this.valoresVencidas.push(this.vencidas[valor]['valor_total']);
                                                            }
                                                            // seteo valores a gráfico
                                                            let listOfLists : number[][] = [];
                                                            listOfLists.push(this.valoresImpagas,
                                                              this.valoresVencidas,
                                                              this.valoresFacturadas);
                                                            this.barChartData3  = listOfLists;
        });
      }
}

