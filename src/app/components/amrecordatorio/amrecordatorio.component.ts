import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Reminder } from '../../interfaces/reminder.interface';
import { Component, OnInit } from '@angular/core';
import {RemindersService } from './../../services/reminders.service';

@Component({
  selector: 'app-amrecordatorio',
  templateUrl: './amrecordatorio.component.html',
})
export class AmrecordatorioComponent implements OnInit {
 recordatorio: any = { };
  /* recordatorio: Recordatorio = {
    fecha_inicio: '',
    fecha_fin: '',
    descripcion: '',
    estado: '',
    tipo: '',
    ids_usuario: [],
    usuarios_permitidos: [],
  }; */
  id: number;


  constructor( private _recordatorioService: RemindersService,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService ) {
      this.route.params.subscribe(
        parametros => {
          // Dependiendo si busca modificar o crear un recordatorio
          if (parametros.id == null) {
            console.log('Agregar');
           this.id = 0;
          }
          else {
            console.log('Editar');
           this.id = parametros['id'];
           this._recordatorioService.getReminder(this.id).subscribe(
                                  data => {this.recordatorio = data[0];
                                            console.log(this.recordatorio);
                                           }
                                  );
                                  console.log(this.recordatorio);

          }
        }
      );
    }
  ngOnInit() {
    this.auth.refreshSession();
  }

  // Metodo para guardar o actualizar recordatorio
  guardar() {
    console.log(this.recordatorio);
     if (this.id === 0 ) {
      // insertando
      const list = [];
      list.push(this.recordatorio.usuarios_permitidos);
      this.recordatorio.usuarios_permitidos = list;
      console.log(this.recordatorio);
      this._recordatorioService.newReminder( this.recordatorio )
      .subscribe(data => { },
      error => console.log(error));
      this.router.navigate(['/recordatorios/']);
    }
    // tslint:disable-next-line:one-line
     else {
      // actualizando
      console.log(this.id);
      this._recordatorioService.updateReminder( this.recordatorio)
      .subscribe(data => {
        console.log(data);
    },
      error => console.log(error));
      this.router.navigate(['/recordatorios/']);
     }
  }

}
