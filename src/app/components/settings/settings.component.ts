import { ParametersService } from '../../services/parameters.service';
import { Settings } from '../../interfaces/settings.interface';
import { CategoriesService } from '../../services/categories.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit {

  categorias: any [];

  settings: any = [];
  fechaAltasBajas = '';

  settingsActualizar: any [] = [];


  constructor(private _categoriasService: CategoriesService,
              private _parametrosService: ParametersService,
              private auth: AuthService) {
                this.auth.refreshSession();
                console.log(localStorage.getItem('access_token'));
               }

  ngOnInit() {
    this._parametrosService.getParametros().subscribe(data => {
      this.settings = data;
      this.fechaAltasBajas = this.settings[1];
      // Quito el parametro de fecha de altas y bajas
      for (const i in this.settings) {
        if (this.settings[i]['nombre'] === 'fecha_calculo_recordatorio') {
          this.fechaAltasBajas = this.settings[i];
          this.settings.splice(i, 1);

        }
      }
    });
    this._categoriasService.getCategories().subscribe(data => {
      this.categorias = data;
    });
  }

  guardar() {
      this._parametrosService.updateParameters( JSON.stringify(this.settings) )
      .subscribe(data => {
        if (data['status'] === 200) {
          alert('Parametros actualizados con exito');
        }
    },
      error => {alert('Hubo un error');
                console.log(error);
    });

  }

}
