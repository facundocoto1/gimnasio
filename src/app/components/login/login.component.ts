import { User } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
// Variable to create and save new User object
  user: User = {
    username: '',
    password: '',
    email: '',
  };
// Variable to send user object and login
  userLog: any = {
    username: '',
    password: ''
  };

// Response from server after login
  response: any = {
    token: '',
    expireIn: 0
  };

  // Variables to control login
  incorrect = false;
  logged = false;

  constructor(private _auth: AuthService,
              private _router: Router,
              private _userService: UsersService) { }
    ngOnInit() {}

  // Metodo para loggear usuario
    loginUser(e) {
      e.preventDefault();
      const username = e.target.elements[0].value;
      const password = e.target.elements[1].value;
      this.userLog.username = username;
      this.userLog.password = password;

      this._userService.loginUser(this.userLog).subscribe(data => {this.response = data;
                                                                      this.response.expireIn = 5000;
                                                                      this._auth.login(this.response);
                                                                      this._auth.setUserLoggedIn();
                                                                      this.logged = this._auth.getUserLoggedIn();
                                                                        if (this.logged === true ) {
                                                                          localStorage.setItem('username', this.userLog.username);
                                                                          this.incorrect = false;
                                                                          this._router.navigate(['home']);
                                                                        }
                                                                      },
                                                          error => {
                                                                     this.incorrect = true;
                                                                   }
                                                          );
  }

    // Method to create a new user
    guardar() {
        this._userService.newUser( this.user )
        .subscribe(data => {
                            alert('Usuario creado con éxito, puede ingresar');
                            this._router.navigate(['landing']);
                           },
                   error => alert('Ocurrio un error, intente nuevamente'));
      }

  }
