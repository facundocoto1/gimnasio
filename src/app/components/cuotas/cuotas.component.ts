import { Bill } from '../../interfaces/bill.interface';
import { Router } from '@angular/router';
import { Component, Directive, OnInit } from '@angular/core';
import { BillsService } from './../../services/bills.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-cuotas',
  templateUrl: './cuotas.component.html',
  styleUrls: []
})
export class CuotasComponent implements OnInit {

  cuotasImpagas: Bill[] = [];
  cuotasVencidas: Bill[] = [];
  cuotasPagas: Bill[] = [];

  cuotaAPagar = [];

  loading = true;

  constructor( private _cuotasService: BillsService,
               private auth: AuthService ) {
              this.auth.refreshSession();
   }

  ngOnInit() {
      this._cuotasService.getBillsUnpaid().subscribe(data => {
      this.cuotasImpagas = data;
    });

      this._cuotasService.getBillsPaid().subscribe(data => {
      this.cuotasPagas = data;
    });

      this._cuotasService.getBillsExpired().subscribe(data => {
      this.cuotasVencidas = data;
    });
    this.loading = false;
    }

    pagar(id: number) {
      const hoy = new Date().toISOString().slice(0, 10);
      this._cuotasService.getBill(id).subscribe(data => {this.cuotaAPagar = data[0];
                                           this.cuotaAPagar['fecha_pago'] = hoy;
                                           console.log('se manda', this.cuotaAPagar);
                                          this._cuotasService.actualizarCuota(this.cuotaAPagar).subscribe( res => {console.log(res); 
                                            this._cuotasService.getBillsUnpaid().subscribe(data => {
                                              this.cuotasImpagas = data;
                                            });

                                              this._cuotasService.getBillsPaid().subscribe(data => {
                                              this.cuotasPagas = data;
                                            });

                                              this._cuotasService.getBillsExpired().subscribe(data => {
                                              this.cuotasVencidas = data;
                                            });
                                          });
                                        });
    }

    cancelarPago(id: number) {
      const hoy = new Date().toISOString().slice(0, 10);
      this._cuotasService.getBill(id).subscribe(data => {this.cuotaAPagar = data[0];
                                           this.cuotaAPagar['fecha_pago'] = null;
                                           console.log('se manda', this.cuotaAPagar);
                                          this._cuotasService.actualizarCuota(this.cuotaAPagar)
                                            .subscribe( res => {console.log(res['status']);
                                            this._cuotasService.getBillsUnpaid().subscribe(data => {
                                              this.cuotasImpagas = data;
                                            });

                                              this._cuotasService.getBillsPaid().subscribe(data => {
                                              this.cuotasPagas = data;
                                            });

                                              this._cuotasService.getBillsExpired().subscribe(data => {
                                              this.cuotasVencidas = data;
                                            });
                                          });
                                        });
    }
}
