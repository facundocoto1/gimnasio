import { Client } from '../../interfaces/client.interface';
import { Router } from '@angular/router';
import { Component, Directive, OnInit } from '@angular/core';
import { ClientsService } from './../../services/clients.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
})
export class ClientesComponent implements OnInit {
  clientes: any[] = [];
  loading = true;

  clienteBaja: Client;

  constructor( private _clientesService: ClientsService,
              private auth: AuthService ) {
    this._clientesService.getClients().subscribe( data => {
      this.clientes = data;
      this.loading = false;
    });
   }

  ngOnInit() {
    console.log(localStorage.getItem('access_token'));
    this.auth.refreshSession();
    }

    bajarCliente (key$: string) {
      this._clientesService.getCliente(key$)
      .subscribe( res => {
        this.clienteBaja = res[0];
        const hoy = new Date().toISOString().slice(0, 10);
        console.log(hoy);
        this.clienteBaja.unsubscription_date = hoy;
        console.log(this.clienteBaja);
        this._clientesService.updateClient(this.clienteBaja, key$).subscribe( resp => {if (resp['status'] = 200) {
                                                                                            alert('Cliente dado de baja con éxito'); }
                                                                                        });
         });
}

buscarCliente (key$: string) {
  if (key$ === '') {
    this._clientesService.getClients().subscribe(res => this.clientes = res);
  }
  else {
  this._clientesService.searchClient(key$).subscribe( res => { this.clientes = res;
    console.log(res); });
}
}
}
