import { ClientsService } from '../../services/clients.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BillsService } from './../../services/bills.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-cuota',
  templateUrl: './cuota.component.html',
})
export class CuotaComponent implements OnInit {
  loading = true;
  nrocliente: any;
  // Nombre del cliente
  cliente: any = {};
  nombre = '';


  cuotasClientePagadas: any[] = [];
  cuotasClienteVencidas: any[] = [];
  cuotasClienteImpagas: any[] = [];

  cuotaAPagar = [];
    constructor( private activatedRoute: ActivatedRoute,
               private _cuotasService: BillsService,
               private _clienteService: ClientsService,
               private auth: AuthService ) {
                 this.auth.refreshSession();
        this.activatedRoute.params.subscribe( params => {
          this.nrocliente = params['id'];
        });
        this._clienteService.getCliente(this.nrocliente).subscribe( data => {
                                                                          this.cliente = data[0];
                                                                        this.nombre = data[0].nombre + ' ' + data[0].apellido;
        });

        this._cuotasService.getBillsPaidClient(this.nrocliente).subscribe( data => {
        this.cuotasClientePagadas = data;
        this.loading = false;
        });

        this._cuotasService.getBillsExpiredClient(this.nrocliente).subscribe( data => {
        this.cuotasClienteVencidas = data;
        this.loading = false;
        });

        this._cuotasService.getBillsUnpiadClient(this.nrocliente).subscribe( data => {
        this.cuotasClienteImpagas = data;
        this.loading = false;
        });
  }

  ngOnInit() {
  }

  pagar(id: number) {
    const hoy = new Date().toISOString().slice(0, 10);
    this._cuotasService.getBill(id).subscribe(data => {this.cuotaAPagar = data[0];
                                         this.cuotaAPagar['fecha_pago'] = hoy;
                                         console.log('se manda', this.cuotaAPagar);
                                        this._cuotasService.actualizarCuota(this.cuotaAPagar).subscribe( res => {console.log(res);
                                          // Recargo cuotas
                                            this._cuotasService.getBillsPaidClient(this.nrocliente).subscribe( data => {
                                            this.cuotasClientePagadas = data;
                                            this.loading = false;
                                            });
                                            this._cuotasService.getBillsExpiredClient(this.nrocliente).subscribe( data => {
                                            this.cuotasClienteVencidas = data;
                                            this.loading = false;
                                            });
                                            this._cuotasService.getBillsUnpiadClient(this.nrocliente).subscribe( data => {
                                            this.cuotasClienteImpagas = data;
                                            this.loading = false;
                                            });
                                        });
                                      });
  }

  cancelarPago(id: number) {
    const hoy = new Date().toISOString().slice(0, 10);
    this._cuotasService.getBill(id).subscribe(data => {this.cuotaAPagar = data[0];
                                         this.cuotaAPagar['fecha_pago'] = null;
                                         console.log('se manda', this.cuotaAPagar);
                                        this._cuotasService.actualizarCuota(this.cuotaAPagar).subscribe( res => {console.log(res);
                                            // Recargo cuotas
                                              this._cuotasService.getBillsPaidClient(this.nrocliente).subscribe( data => {
                                              this.cuotasClientePagadas = data;
                                              this.loading = false;
                                              });
                                              this._cuotasService.getBillsExpiredClient(this.nrocliente).subscribe( data => {
                                              this.cuotasClienteVencidas = data;
                                              this.loading = false;
                                              });
                                              this._cuotasService.getBillsUnpiadClient(this.nrocliente).subscribe( data => {
                                              this.cuotasClienteImpagas = data;
                                              this.loading = false;
                                              });
                                        });
                                      });
  }

}
