import { AuthService } from '../../services/auth.service';
import { Address } from '../../interfaces/address.interface';
import { Telephone } from '../../interfaces/telephone.interface';
import { CategoriesService } from '../../services/categories.service';
import { Component, OnInit } from '@angular/core';
import { Form, FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Client } from '../../interfaces/client.interface';
import { ClientsService  } from '../../services/clients.service';
import { AddressService  } from '../../services/address.service';
import { TelephonesService  } from '../../services/telephones.service';



@Component({
  selector: 'app-amcliente',
  templateUrl: './amcliente.component.html',
  styles: []
})
export class AmclienteComponent implements OnInit {

  cliente: Client = {
    id: 0,
    name: '',
    lastname: '',
    birthdate: '',
    number_identity_card: '',
    observaciones: '',
    email: '',
    sex: '',
    subscription_date:  new Date().toISOString().slice(0, 10),
    address: 1,
    expiration_date: 1,
    categories: [],
    phones: [],
    picture: ''
  };

  telefono: Telephone = {
    number: 0,
    area_code: 0,
    country_code: 0,
    telephone_type: '',
    client_id: 0,
  };

   domicilio: Address = {
    street: '',
    id: 0
  };

  formas: FormGroup;
  telefonoForm: FormGroup;
  domicilioForm: FormGroup;
  categorias: any [];

  id: string;

  idInsertar: number;

  constructor( private _clienteService: ClientsService,
               private _categoriasService: CategoriesService,
               private _domicilioService: AddressService,
               private _telefonoService: TelephonesService,
               private router: Router,
               private route: ActivatedRoute,
               private auth: AuthService ) {

                this.formas = new FormGroup({
                  'nombre': new FormControl('', Validators.required),
                  'apellido': new FormControl(''),
                  'fecha_nacimiento': new FormControl(''),
                  'tipo_documento': new FormControl(''),
                  'numero_documento': new FormControl(''),
                  'observaciones': new FormControl(''),
                  'email': new FormControl(''),
                  'sexo': new FormControl(''),
                  'domicilio': new FormControl(''),
                  'dia_vencimiento': new FormControl(''),
                  'fecha_alta': new FormControl(new Date().toISOString().slice(0, 10)),
                  'fecha_baja': new FormControl(null),
                  'id': new FormControl(''),
                   'categorias': new FormArray([
                     new FormControl(this.categorias, Validators.required)
                   ])
                   });

                  this.telefonoForm = new FormGroup({
                    'codigo_pais': new FormControl('', Validators.required),
                    'codigo_area': new FormControl(''),
                    'tipo': new FormControl (''),
                    'numero': new FormControl(''),
                    'id': new FormControl(),
                    'cliente': new FormControl()
                  });

                  this.domicilioForm = new FormGroup({
                    'calle': new FormControl('', Validators.required),
                    'numero': new FormControl(''),
                    'piso': new FormControl(''),
                    'departamento': new FormControl(''),
                    'id': new FormControl()
                  });

                 this.route.params.subscribe(
                   parametros => {
                     this._categoriasService.getCategories().subscribe(data => {
                       this.categorias = data;
                     });

                     if (parametros.id == null) {
                      this.id = 'nuevo';
                     }
                     else {
                      this.id = parametros['id'];
                      this._clienteService.getCliente(this.id).subscribe( client => {
                        this.cliente = client[0];
                        console.log(this.cliente);
                        this.formas.setValue(this.cliente);
                      });
                      // obtengo su direccion
                      const idCliente: number = Number(this.id);
                      this._domicilioService.getAddress(idCliente).subscribe(dom => {this.domicilio = dom;
                                                                                       console.log(this.domicilio);
                                                                                       this.domicilioForm.setValue(this.domicilio);
                                                                                      });
                      // obtengo su telefono

                      this._telefonoService.getTelefono(idCliente).subscribe(tel => {this.telefono = tel[0];
                        this.telefonoForm.setValue(this.telefono);
                       });
                     }
                   }
                 );
                   // console.log(this.formas);
               }

  ngOnInit() {
    this.auth.refreshSession();
  }

  guardar() {
    // console.log(this.cliente);
    console.log(this.formas.value);
     if (this.id === 'nuevo' ) {
      // insertando
      this._clienteService.newClient( this.formas.value )
      .subscribe(data => {
                          this.idInsertar = data.id;
                          console.log(this.idInsertar);
                          this._domicilioService.newAddress(this.domicilioForm.value, this.idInsertar).subscribe();
                          console.log(this.domicilioForm.value);
                          this._telefonoService.newTelephone(this.telefonoForm.value, this.idInsertar).subscribe();
    }, error => console.log(error));
    }
    // tslint:disable-next-line:one-line
     else {
      // actualizando
      this._telefonoService.actualizarTelefono(this.telefonoForm.value, this.telefonoForm.get('id').value).subscribe();
      this._domicilioService.updateAddress(this.domicilioForm.value, this.id).subscribe();
      this._clienteService.updateClient( this.formas.value, this.id )
      .subscribe(data => {
        console.log(data);
    },
      error => console.log(error));
     }
     this.router.navigate(['/clientes/']);
    }
  agregarCategoria() {
( <FormArray>this.formas.controls['categorias']).push(
  new FormControl('', Validators.required)
);
 }

 agregarTelefono() {
  ( <FormArray>this.formas.controls['telefonos']).push(
    new FormControl('', Validators.required) );
   }
  }
