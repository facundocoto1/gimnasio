import { UsersService } from '../../services/users.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styles: []
})
export class AccountComponent implements OnInit {
  // Variable to save user currently logged
  profile: any = {};
  // Variable to create and update User object
  user: any= {};
  // Variable that contains response http from server
  respuesta = 0;

  constructor( private _auth: AuthService,
               private _userService: UsersService) {
                 // Refresh the session (time expired) of user logged
                this._auth.refreshSession();
                // Get profile of user logged
                this._userService.getUser(localStorage.getItem('username'))
                    .subscribe(data => { this.profile = data[0]; });
                }
  ngOnInit() {}

  // Method to update password
    savePassword() {
      this.user.username = localStorage.getItem('username');
      this.user.password = this.profile.password;
      this._userService.updateUser(this.user)
        .subscribe(res => {
                            this.respuesta = res['status'];
                            if (this.respuesta === 200) {
                              alert('Contraseña actualizado con éxito');
                            }},
                   error => {
                            this.respuesta = error['status'];
                            alert('La contraseña debe tener letras y numeros. Intente nuevamente');
                            });
    }
  // Method to update email
    guardarCorreo() {
      this.user.username = localStorage.getItem('username');
      this.user.email = this.profile.email;
      this._userService.updateUser(this.user)
        .subscribe(res => {
                            this.respuesta = res['status'];
                            if (this.respuesta === 200) {
                              alert('Correo actualizado con éxito');
                            }});
    }
  }

