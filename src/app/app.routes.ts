import { LandingComponent } from './components/landing/landing.component';
import { CanActivate, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { RecordatoriosComponent } from './components/recordatorios/recordatorios.component';
import { HomeComponent } from './components/home/home.component';
import { CuotasComponent } from './components/cuotas/cuotas.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { CuotaComponent } from './components/cuota/cuota.component';
import { AmclienteComponent } from './components/amcliente/amcliente.component';
import { AccountComponent } from './components/account/account.component';
import { AmrecordatorioComponent } from './components/amrecordatorio/amrecordatorio.component';
import { AmcategoriaComponent } from './components/amcategoria/amcategoria.component';



import { AuthGuardService } from './services/auth-guard.service';




const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'menu', component: MenuComponent, canActivate: [ AuthGuardService ] },
    { path: 'clientes', component: ClientesComponent, canActivate: [ AuthGuardService ] },
    { path: 'recordatorios', component: RecordatoriosComponent, canActivate: [ AuthGuardService ]},
    { path: 'cuotas', component: CuotasComponent, canActivate: [ AuthGuardService ] },
    { path: 'home', component: HomeComponent, canActivate: [ AuthGuardService ] },
    { path: 'settings', component: SettingsComponent, canActivate: [ AuthGuardService ] },
    { path: 'amcliente', component: AmclienteComponent, canActivate: [ AuthGuardService ] },
    { path: 'amcliente/:id', component: AmclienteComponent, canActivate: [ AuthGuardService ] },
    { path: 'cliente/:id', component: ClienteComponent, canActivate: [ AuthGuardService ] },
    { path: 'cuota/:id', component: CuotaComponent, canActivate: [ AuthGuardService ] },
    { path: 'account', component: AccountComponent, canActivate: [ AuthGuardService ] },
    { path: 'amrecordatorio', component: AmrecordatorioComponent, canActivate: [ AuthGuardService ] },
    { path: 'amrecordatorio/:id', component: AmrecordatorioComponent, canActivate: [ AuthGuardService ] },
    { path: 'amcategoria', component: AmcategoriaComponent, canActivate: [ AuthGuardService ] },
    { path: 'amcategoria/:id', component: AmcategoriaComponent, canActivate: [ AuthGuardService ] },
    { path: 'landing', component: LandingComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'landing' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
