import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LOCALE_ID } from '@angular/core';


// Rutas
import { APP_ROUTING } from './app.routes';

// Services
import {ClientsService} from './services/clients.service';
import { RemindersService } from './services/reminders.service';
import {BillsService} from './services/bills.service';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import { CategoriesService } from './services/categories.service';
import { ParametersService } from './services/parameters.service';
import { UsersService } from './services/users.service';
import { TelephonesService } from './services/telephones.service';
import { AddressService } from './services/address.service';
import { ReportsService } from './services/reports.service';

// Idioma para pipe

// Graficos
import { ChartsModule } from 'ng2-charts';


// Pipes
import { KeysPipe } from './pipes/keys.pipe';
import { TimesPipe } from './pipes/times.pipe';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { RecordatoriosComponent } from './components/recordatorios/recordatorios.component';
import { HomeComponent } from './components/home/home.component';
import { CuotasComponent } from './components/cuotas/cuotas.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { CuotaComponent } from './components/cuota/cuota.component';
import { AmclienteComponent } from './components/amcliente/amcliente.component';
import { AccountComponent } from './components/account/account.component';
import { AmrecordatorioComponent } from './components/amrecordatorio/amrecordatorio.component';
import { AmcategoriaComponent } from './components/amcategoria/amcategoria.component';
import { LandingComponent } from './components/landing/landing.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    ClientesComponent,
    RecordatoriosComponent,
    HomeComponent,
    CuotasComponent,
    SettingsComponent,
    ClienteComponent,
    CuotaComponent,
    AmclienteComponent,
    KeysPipe,
    TimesPipe,
    AccountComponent,
    AmrecordatorioComponent,
    AmcategoriaComponent,
    LandingComponent,
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpModule,
    FormsModule,
    ChartsModule,
    ReactiveFormsModule
  ],
  providers: [
    ClientsService,
    BillsService,
    AuthService,
    AuthGuardService,
    RemindersService,
    CategoriesService,
    ParametersService,
    UsersService,
    AddressService,
    TelephonesService,
    ReportsService,
    {provide: LOCALE_ID, useValue: 'es'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
